# IPA Fall Days Tutorial
## Using NLP for Software Engineering

One of the main concepts in software engineering is version control. A widely used implementation of version control is Git, often in combination with the web application Github. Within Github people can upload code as well as discuss project goals, report bugs etc. The main tool for this discussion is the **issue tracker**, where people can post messages about the project and comment on messages posted earlier.

The issue tracker is the topic of the tutorial. We will use a dataset with Github issues to apply Natural Language Processing (NLP) methods for data exploration and machine learning.

## Step 1: Data exploration
We use the GHTorrent dataset, created by Georgios Gousios. For this tutorial I created a small and preprocessed version with issues, comments and issue events from August-November 2015 in CSV format. Download the data here: https://git.science.uu.nl/M.P.Schraagen/ipa-fall-days/-/raw/b206cb3371d9f3dabc02663db40d527a4a1dfab7/IPA_issue_data.7z

There are three files in the download, one with the issues, one with comments on the issues, and one with issue events. These events are things like an issue being mentioned, given a label, being subscribed to, made part of a milestone or assigned to a particular contributor.

**Exercise 1.1**  
Using a scripting/programming language of your choice, create frequency counts of the following properties:

- events per issue
- event label distribution
- comments per issue

Note that issues, comments and events are linked by the unique identifier `issue_id`. There is also a field `issue_nr`, but this is not a unique identifier, it is the sequence number of the issue within the repository (for example https://github.com/gousiosg/github/github-mirror/issues/95 has `issue_nr` 95). The combination `owner/project/issue_nr` is again a unique identifier.

Visualize these counts in a histogram/bar chart. Use binning or cutoff outliers if necessary.

If you sue the `csv` library in Python you should increase the maximum length of the fields, as some issues are rather long:
```python
import csv
import sys
csv.field_size_limit(sys.maxsize)
```

**Exercise 1.2.1**  
Assume you are a contributor and you want to know about issues that people feel particularly strongly about, so you can address them. Use a sentiment analysis tool to find issue titles, issue body text, or comment text with a negative polarity (e.g., -0.75 or lower). For Python you can use the `textblob` library that has an easy to use sentiment analysis function. Try to find some examples of important issues. Check what works best: issue titles, issue body, or comments.

**Exercise 1.2.2**  
You will notice that many negative polarity results are false positives. Try to filter out some problematic hits. As an example of an NLP approach you can filter out non-English text. Use for example the `langid` library for Python for this. Other approaches include removing text that has code blocks, or keeping the text but removing the code blocks before sentiment analysis. You can also experiment with length, i.e., keep, remove or cutoff text based on length thresholds.

## Step 2: Classification
In this step we focus on the events _milestoned_ and _assigned_, with the assumption that these are important issues to work on and they should be identified early. We will try to develop a classifier that based on the initial text of an issue can predict if this issue will eventually become milestoned or assigned.

**Exercise 2.1**  
Create a new CSV file with for each issue the title, body, and three (boolean) columns that indicate whether the issue is milestoned, assigned, or both. Use the `issue_id` to link the rows from the issue file and the event file. Alternatively you can create separate CSV files for the titles and body, this makes it easier to view and process the files and to create subsets to reduce the size.

Try to balance the classes, which means you need to discard rows with the `False` label.

**Exercise 2.2**  
Train a number of traditional machine learning models. You can use a programming language of your choice that offers machine learning libraries, however Python is recommended, particularly the `sklearn` library.

Use for example the following algorithms:

- Dummy classifier: this classifier chooses the majority label, so for a binary balanced task it will have 50% accuracy. This is a quick way to check if your machine learning pipeline works correctly.
- Naive Bayes
- Logistic Regression
- Random Forest. Note: this algorithm is _very_ slow on large datasets. Create a subset of the data if you want to try this classifier, and also use parallel training (the `n_jobs` parameter in sklearn)
- Neural Network (MultiLayer Perceptron). This algorithm is also very slow.

Imports for `sklearn`:

```python
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.dummy import DummyClassifier
```

Further tips for `sklearn`: Train a classifier using the `.fit()` function. Run the trained classifier on the test set: use the `.predict()` function and compare the results to the known labels. Input representation: use `sklearn.feature_extraction.text.CountVectorizer` for a bag-of-words representation with `.fit_transform()` (train set) and `.transform()` (test set). Split the data in train and test: use `sklearn.model_selection.train_test_split()`.

**Exercise 2.3**  
You will find that the performance is better than random, but not great. We can try using the BERT pretrained language model to see if it does any better. This works best with a GPU. A free and easy to use environment with a GPU is Google Colab. Create a new notebook on https://colab.research.google.com and choose the GPU runtime type in the menu.

To upload files in Colab you can use the following snippet:

```python
from google.colab import files
uploaded = files.upload()
```

Note that upload can be slow, consider reducing the file before uploading.

BERT is available through the `transformers` Python library. Install this library first using `!pip`. Installation and useful imports:

```python
!pip install transformers
import torch
from torch.utils.data import TensorDataset, Subset, DataLoader, Dataset, RandomSampler, SequentialSampler
import pandas as pd
import numpy as np
from transformers import BertTokenizerFast, BertForSequenceClassification
from transformers import Trainer, TrainingArguments
```

A classification model can be trained using the following model:

```python
BertForSequenceClassification.from_pretrained("bert-base-uncased", num_labels = 2).to("cuda")
```

Note that fine-tuning can take a long time, limit the size of the dataset to a few thousand rows for faster training.

Before training you need to setup the tokenizer using the same model (for example `bert-base-uncased`). The easiest way to get the data in the right format is to use `Trainer` with `TrainingArguments`. The `Trainer` class exposes the functions` .train()` and `.evaluate()` for training and testing. You can subclass `torch.utils.data.Dataset` to load the data in the right format for `Trainer`.

Note that BERT predicts probabilities for both labels, and of course you want only the highest one. The following metrics function can be used with `Trainer()` to select the label with the highest probability and compare the predicted labels to the actual labels:

```python
def compute_metrics(pred):
    labels = pred.label_ids
    preds = pred.predictions.argmax(-1)
    # calculate accuracy using sklearn's function
    acc = accuracy_score(labels, preds)
    return {
      'accuracy': acc,
    }
```

## Guess the label
You will find that also BERT has trouble with this task, although allowing for longer training will most likely be able to improve the results. Nonetheless, this means that the task of predicting milestones and issue assignments is difficult, and probably people will also have problems performing this task.

**Exercise 3**  
Create a small game to see who is better in predicting: you or a machine learning algorithm. The game consists of the following stages:

1. Show a title and body for an issue
2. Ask the user if this will be a milestone
3. Classify with one of the algorithms you have implemented
4. Whoever is correct (the user or the algorithm) gets a point
